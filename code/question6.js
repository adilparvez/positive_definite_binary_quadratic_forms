'use strict';

var fs = require('fs');
var bqf = require('./binary-quadratic-forms');
var q5 = require('./question5');

// Get residues of primes less than n (excluding those that divide d)
// represented by forms of discriminant d mod |d|.
function getPrimeResidues(d, n) {
  var primes = q5.sieve(n);
  var dFactors = bqf.uniqueFactors(d);
  var primitive = bqf.getForms(d, 'primitive');
  var forms = [];
  primitive.forEach(function(form) {
    forms.push({
      form: form,
      primeResidues: []
    });
  });
  primes.forEach(function(p) {
    if (dFactors.indexOf(p) === -1) {
      var representingForms = bqf.getAllFormsRepresentingPrime(d, p);
      if (typeof representingForms !== 'string') {
        for (var i = 0; i < representingForms.length; ++i) {
          for (var j = 0; j < forms.length; ++j) {
            if (bqf.equal(forms[j].form, representingForms[i].reduced)) {
              var residues = forms[j].primeResidues;
              if (residues.indexOf(p % Math.abs(d)) === -1) {
                forms[j].primeResidues.push(p % Math.abs(d));
              }
            }
          }
        }
      }
    }
  });
  forms.forEach(function(form) {
    form.primeResidues.sort(function(a, b) {
      return a - b;
    });
  });
  return forms;
}


function genera(path) {
  fs.writeFileSync(path, 'Investigating the number of genera of ' +
    'primitive forms.\n');
  fs.appendFileSync(path, 'd = -400\n');
  var forms = getPrimeResidues(-400, 10000);
  for (var i = 0; i < forms.length; ++i) {
    var form = forms[i];
    fs.appendFileSync(path, bqf.makePretty(form.form) + '\n' +
      '[' + form.primeResidues + ']\n');
  }
  fs.appendFileSync(path, 'd = -56\n');
  var forms = getPrimeResidues(-56, 10000);
  for (var i = 0; i < forms.length; ++i) {
    var form = forms[i];
    fs.appendFileSync(path, bqf.makePretty(form.form) + '\n' +
      '[' + form.primeResidues + ']\n');
  }
}

genera('question6-genera.txt');
