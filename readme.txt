This code for this project was written in server-side javascript (Node.js).

Some pictures are drawn with python and R.

If it isn't already installed and isn't in the package manager here are some instructions: http://blog.teamtreehouse.com/install-node-js-npm-linux

If it is in the package manager, just install it from there.

To run the project, cd to the code directory and type node <fileNameHere> into the terminal.

Files included are:
binary-quadratic-forms.js
questionX.js - output for question X
question2-plot.py, question2-regression.r - draw pictures