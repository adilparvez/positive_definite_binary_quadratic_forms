'use strict';

var fs = require('fs');
var bqf = require('./binary-quadratic-forms');

var ds = [-3, -4, -7, -8, -11, -15, -19, -20, -23, -24, -31, -35, -39, -40, -43,
  -47, -51, -52, -55, -56, -59, -67, -68, -71, -79, -83, -84, -87, -88, -91, -95];

function ambiguousForms(path) {
  fs.writeFileSync(path, 'Number of ambiguous forms with given fundamental discriminant.\n');
  for (var i = 0; i < ds.length; ++i) {
    var d = ds[i];
    fs.appendFileSync(path, 'd = ' + d + ', factors = [' + bqf.uniqueFactors(d) +
      '], numberOfForms = ' + bqf.getForms(d, 'primitive ambiguous').length + '.\n');
  }
}

ambiguousForms('question4-ambiguous-forms.txt');