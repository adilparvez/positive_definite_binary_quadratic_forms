'use strict';

var fs = require('fs');
var bqf = require('./binary-quadratic-forms');

var dpPairs = [
  {
    d: -759,
    p: 6101
  },
  {
    d: -831,
    p: 2251
  },
  {
    d: -1343,
    p: 2411 
  }
];

function formsRepresentingPrimes(path) {
  fs.writeFileSync(path, 'Forms of given discriminant representing given prime.\n');

  fs.appendFileSync(path, 'd = -759, p = 6101\n');
  var forms = bqf.getAllFormsRepresentingPrime(-759, 6101);
  for (var i = 0; i < forms.length; ++i) {
    fs.appendFileSync(path, bqf.prettyReduce(forms[i].original) + '\n');
  }

  fs.appendFileSync(path, 'd = -831, p = 2251\n');
  fs.appendFileSync(path, bqf.getAllFormsRepresentingPrime(-831, 2251) + '\n');

  fs.appendFileSync(path, 'd = -1343, p = 2411\n');
  var forms = bqf.getAllFormsRepresentingPrime(-1343, 2411);
  for (var i = 0; i < forms.length; ++i) {
    fs.appendFileSync(path, bqf.prettyReduce(forms[i].original) + '\n');
  }
}

formsRepresentingPrimes('question5-forms-representing-primes.txt');

function sieve(n) {
  var array = [];
  var limit = Math.sqrt(n);
  var primes = [];
  for (var i = 0; i < n; ++i) {
    array.push(true);
  }
  for(var i = 2; i <= limit; ++i) {
    if (array[i]) {
      for (var j = i * i; j < n; j += i) {
        array[j] = false;
      }
    }
  }
  for (var i = 2; i < n; ++i) {
    if (array[i]) {
      primes.push(i);
    }
  }
  return primes;
}

// Proportion of primes less than nrepresented by each form of discriminant d.
function getProportions(d, n) {
  var primes = sieve(n);
  var primitiveForms = bqf.getForms(d, 'primitive');
  var imprimitiveForms = bqf.getForms(d, 'imprimitive');
  var forms = [];
  primitiveForms.forEach(function(primitive) {
    forms.push({
      form: primitive,
      type: 'primitive',
      count: 0
    });
  });
  imprimitiveForms.forEach(function(imprimitive) {
    forms.push({
      form: imprimitive,
      type: 'imprimitive',
      count: 0
    });
  });
  primes.forEach(function(p) {
    var representingForms = bqf.getAllFormsRepresentingPrime(d, p);
    if (typeof representingForms !== 'string') {
      for (var i = 0; i < representingForms.length; ++i) {
        for (var j = 0; j < forms.length; ++j) {
          if (bqf.equal(forms[j].form, representingForms[i].reduced)) {
            forms[j].count++;
          }
        }
      }
    }
  });
  for (var i = 0; i < forms.length; ++i) {
    forms[i].count /= primes.length;
  }
  return forms;
}

function proportions(path, limit, ds) {
  fs.writeFileSync(path, '');
  ds.forEach(function(d) {
    fs.appendFileSync(path, 'Proportion of primes less than ' + limit +
      ' represented by forms of discriminant ' + d + '.\n');
    var forms = getProportions(d, limit);
    for (var i = 0; i < forms.length; ++i) {
      var form = forms[i];
      fs.appendFileSync(path, bqf.makePretty(form.form) + ' ' + form.type +
        ', proportion = ' + form.count + '\n');
    }
  });
}

var ds = [-24, -56, -400];
proportions('question5-proportions.txt', 10000, ds);

exports.sieve = sieve;
