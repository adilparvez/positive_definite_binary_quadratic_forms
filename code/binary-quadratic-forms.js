'use strict';

// Calculate discriminant of binary quadratic form represented by (a, b, c).
function discriminant(a, b, c) {
  return b*b - 4*a*c;
}

// If a form is reduced then |b| <= a <= c. Get bound on a given the
// discriminant d. Can then determine c from a, b, d.
function getBound(d) {
  return Math.sqrt(-d/3);
}

// Check if form is reduced.
function isReduced(f) {
  //var notReduced = (f.c < f.a) || (Math.abs(f.b) > f.a) || (f.a === -f.b) || (f.a === f.c && f.b < 0);
  return (-f.a < f.b && f.b <= f.a && f.a < f.c) ||
    (0 <= f.b && f.b <= f.a && f.a === f.c);
  // return !notReduced;
}

// Calculate the gcd of an array of integers.
function gcd(A) {
  var x = (A[0] < 0) ? -A[0] : A[0];
  for (var i = 1; i < A.length; ++i) {
    var y = (A[i] < 0) ? -A[i] : A[i];
    while (x && y) {
      if (x > y) {
        x %= y;
      } else {
        y %= x;
      }
    }
    x += y;
  }
  return x;
}

// Check if a form is primitive.
function isPrimitive(f) {
  return gcd([f.a, f.b, f.c]) === 1;
}

// Calculate n mod m.
function mod(n, m) {
        return ((n % m) + m) % m;
}

// Check if an integer is a discriminant.
function isDiscriminant(d) {
  return (mod(d, 4) === 0) || (mod(d, 4) === 1);
}

// Check if forms equal.
function equal(f, g) {
  return (f.a === g.a) &&(f.b === g.b) && (f.c === g.c);
}

// Calculate c given d, a, b.
function getC(d, a, b) {
  return (b*b - d) / (4*a);
}

// Get all of the reduced forms that have given discriminant d. Type to
// indicate wether to only return primitive forms, imprimitive or both.
function getForms(d, type) {
  if ((d >= 0) || !isDiscriminant(d)) {
    throw 'Invalid discriminant. Must be negative and zero or one mod four.';
  }
  if (['primitive', 'imprimitive', 'both', 'primitive ambiguous'].indexOf(type) === -1) {
    throw 'Invalid type. Must be one of \'primitive\', \'imprimitive\' ' +
      ',\'both\' or \'primitive ambiguous\'.';
  }
  var forms = [];
  var bound = getBound(d);
  for (var a = 1; a <= bound; ++a) {
    for (var b = -a; b <= a; ++b) {
      var f = {
        a: a,
        b: b,
        c: getC(d, a, b)
      }
      if (Number.isInteger(f.c) && isReduced(f)) {
        switch (type) {
          case 'primitive':
            if (isPrimitive(f)) {
              forms.push(f);
            }
            break;
          case 'imprimitive':
            if (!isPrimitive(f)) {
              forms.push(f);
            }
            break;
          case 'both':
            forms.push(f);
            break;
          case 'primitive ambiguous':
            var reduced = reduce({
              a: f.a,
              b: Math.abs(f.b),
              c: f.c
            }).reduced;
            var negReduced = reduce({
              a: f.a,
              b: -Math.abs(f.b),
              c: f.c
            }).reduced;
            if (isPrimitive(reduced) && equal(reduced, negReduced)) {
              forms.push(reduced);
            }
        }
      }
    }
  }
  return forms;
}

// Calculate class number.
function h(d) {
  return getForms(d, 'primitive').length;
}

// Calculate d with |d| smallest s.t. h(d) = n for given n.
function d(n) {
  var d = -1;
  while (true) {
    if (isDiscriminant(d)) {
      if (h(d) === n) {
        return d;
      }
    }
    --d;
  }
}

// Apply transformation S to form.
function S(f) {
  return {
    a: f.c,
    b: -f.b,
    c: f.a
  };
}

// Apply transformation T+ to form.
function Tp(f) {
  return {
    a: f.a,
    b: f.b + 2*f.a,
    c: f.a + f.b + f.c
  };
}

// Apply transformation T- to form.
function Tm(f) {
  return {
    a: f.a,
    b: f.b - 2*f.a,
    c: f.a - f.b + f.c
  };
}

// Reduce given form and give sequence of transformations applied,
// as well as intermediate forms.
function reduce(f) {
  var sequence = {
    original: f,
    forms: [],
    transformations: [],
    reduced: null
  };
  sequence.forms.push(f);
  while (true) {
    if (f.a > f.c) {
      f = S(f);
      sequence.forms.push(f);
      sequence.transformations.push('S');
    } else if (f.a < f.c && Math.abs(f.b) > f.a) {
      if (Math.abs(Tp(f).b) < Math.abs(Tm(f).b)) {
        f = Tp(f);
        sequence.forms.push(f);
        sequence.transformations.push('T+');
      } else {
        f = Tm(f);
        sequence.forms.push(f);
        sequence.transformations.push('T-');
      }
    } else {
      break;
    }
  }
  if (f.b === -f.a) {
    f = Tp(f);
    sequence.forms.push(f);
    sequence.transformations.push('T+');
  }
  if (f.a === f.c && f.b < 0) {
    f = S(f);
    sequence.forms.push(f);
    sequence.transformations.push('S');
  }
  if (!isReduced(f)) {
    throw "Bug.";
  }
  sequence.reduced = f;
  return sequence;
}

// Pretty print form.
function makePretty(f) {
  return '(' + f.a + ', ' + f.b + ', ' + f.c + ')';
}

// Pretty print reduction of form.
function prettyReduce(f) {
  var sequence = reduce(f);
  var pretty = makePretty(sequence.forms[0]) + '\n';
  for (var i = 1; i < sequence.transformations.length; ++i) {
    pretty += '=[' + sequence.transformations[i] + ']=> ' +
      makePretty(sequence.forms[i + 1]) + '\n';
  }
  return pretty;
}

// Get array of prime factors of a number.
function getFactors(n) {
  if (n === 0) {
    return [];
  }
  n = Math.abs(n);
  var factors = [];
  while (n % 2 === 0) {
    factors.push(2);
    n /= 2;
  }
  var divisor = 3;
  while (n !== 1) {
    while (n % divisor === 0) {
      factors.push(divisor);
      n /= divisor;
    }
    divisor += 2;
  }
  return factors;
}

// Remove duplicate numbers from an array.
function dropDuplicates(array) {
  var uniqueElements = [];
  array.forEach(function(element) {
    if (uniqueElements.indexOf(element) === -1) {
      uniqueElements.push(element);
    }
  });
  return uniqueElements;
}

// Get unique prime factors of a number.
function uniqueFactors(n) {
  return dropDuplicates(getFactors(n));
}

// Find all x s.t x^2 = d (mod n). 
function modSqrt(d, n) {
  d = mod(d, n);
  var roots = [];
  for (var x = 0; x < n; ++x) {
    if (mod(x*x, n) === d) {
      roots.push(x);
    }
  }
  return roots;
}

// Get next number with same residue mod n. Alternating in sign and
// increasing in magnitude.
function getNextNumberWithSameResidue(x, n) {
  var offset = Math.floor(Math.abs(x) / n);
  return (x >= 0) ? (x - (2*offset + 1)*n) : (x + (2*offset + 2)*n);
}

// Get all reduced forms of discriminant d that represent prime p.
function getAllFormsRepresentingPrime(d, p) {
  // Get square roots of d mod 4p.
  var roots = modSqrt(d, 4*p);
  if (roots.length === 0) {
    return 'Prime p represented by some form if and only if d is a quadratic residue mod 4p.\n';
  }
  var forms = [];
  roots.forEach(function(b) {
    var c = getC(d, p, b);
    forms.push(reduce({
      a: p,
      b: b,
      c: c
    }));
  });
  var uniqueForms = [];
  forms.forEach(function(form) {
    var unique = true;
    uniqueForms.forEach(function(uniqueForm) {
      if (equal(form.reduced, uniqueForm.reduced)) {
        unique = false;
      }
    });
    if (unique) {
      uniqueForms.push(form)
    }
  });
  return uniqueForms;
}

exports.isDiscriminant = isDiscriminant;
exports.equal = equal;
exports.getForms = getForms;
exports.h = h;
exports.d = d;
exports.makePretty = makePretty;
exports.prettyReduce = prettyReduce;
exports.uniqueFactors = uniqueFactors;
exports.getAllFormsRepresentingPrime = getAllFormsRepresentingPrime;
