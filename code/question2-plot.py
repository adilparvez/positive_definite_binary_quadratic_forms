# Plot |d| against n.

import matplotlib.pyplot as plt
import numpy as np

def draw(path_to_csv):
	data = np.genfromtxt(path_to_csv, delimiter=',', names=['n', 'd'], skip_header=1)
	plt.plot(data['n'], data['d'])
	plt.xlabel('n')
	plt.ylabel('|d|')
	plt.show()

draw('question2-discriminants.csv')