'use strict';

var fs = require('fs');
var bqf = require('./binary-quadratic-forms');

// Output class numbers.
function classNumbers(path) {
  fs.writeFileSync(path, 'Class numbers for discriminants -100 <= d < 0.\n');
  for (var d = -1; d >= -100; --d) {
    if (bqf.isDiscriminant(d)) {
      fs.appendFileSync(path, 'h(' + d + ') = ' + bqf.h(d) + '\n');
    }
  }
}

// Output d with smallest |d| for a given class number.
function discriminants(path) {
  fs.writeFileSync(path, 'Discriminants d with |d| smallest s.t. h(d) = n for 1 <= n <= 100.\n');
  fs.writeFileSync
  for (var n = 1; n <= 100; ++n) {
    fs.appendFileSync(path, 'n = ' + n + ', d = ' + bqf.d(n) + '\n');
  }
}

// Output discriminants to csv file for plotting purposes.
function csv(path) {
  fs.writeFileSync(path, 'n,d\n');
  for (var n = 1; n <= 100; ++n) {
    fs.appendFileSync(path, '' + n + ',' + (-bqf.d(n)) + '\n');
  }
}

// Use to investigate class numbers of square times fundamental discriminant.
function h(d, kMax) {
  for (var k = 1; k <= kMax; ++k) {
    console.log('h(' + d + '*' + k + '^2) = h(' + d*k*k + ') = ' + bqf.h(d*k*k));
  }
}

classNumbers('question2-class-numbers.txt');
discriminants('question2-discriminants.txt');
csv('question2-discriminants.csv');
