'use strict';

var fs = require('fs');
var bqf = require('./binary-quadratic-forms');

// Output reduced forms.
function reducedForms(path) {
  fs.writeFileSync(path, 'All reduced forms for discriminants -32 <= d < 0.\n');
  for (var d = -1; d >= -32; --d) {
    if (bqf.isDiscriminant(d)) {
      fs.appendFileSync(path, 'Discriminant d = ' + d + '.\n');
      var primitiveForms = bqf.getForms(d, 'primitive');
      var imprimitiveForms = bqf.getForms(d, 'imprimitive');
      for (var i = 0; i < primitiveForms.length; ++i) {
        fs.appendFileSync(path, bqf.makePretty(primitiveForms[i]) + ' primitive.\n');
      }
      for (var i = 0; i < imprimitiveForms.length; ++i) {
        fs.appendFileSync(path, bqf.makePretty(imprimitiveForms[i]) + ' imprimitive.\n');
      }
    }
  }
}

reducedForms('question1-reduced-forms.txt');