'use strict';

var fs = require('fs');
var bqf = require('./binary-quadratic-forms');

// Output reduction sequence.
function reductions(path) {
  fs.writeFileSync(path, 'Reduction of (134, 651, 791).\n');
  fs.appendFileSync(path, bqf.prettyReduce({
    a:134,
    b:651,
    c:791
  }) + '\n');
  fs.appendFileSync(path, 'Reduction of (3683, 9513, 6143).\n');
  fs.appendFileSync(path, bqf.prettyReduce({
    a:3683,
    b:9513,
    c:6143
  }) + '\n');
}

reductions('question3-reductions.txt');